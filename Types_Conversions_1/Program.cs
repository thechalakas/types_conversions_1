﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Types_Conversions_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int hello_there = 20;

            //this would be implict conversion has it is done automatically
            double hello_there_double = hello_there;

            //this would be explicity conversion as I am explicityly specifying that yeah, the type has to be covnerted to double
            double hello_there_double_explicit = (double)hello_there;

            //this would be conversion with a helper i.e. Convert.To helper
            double hello_there_double_helper = Convert.ToDouble(hello_there);

            
            complicated_water_bottle temp = new complicated_water_bottle();
            //two instances of custom casting 

            //implicitly casting a complicated_water_bottle to an int type
            int hello_there_custom_int = temp;
            //explicityly casting a complicated_water_bottle to an double type
            double hello_there_custom_double = (double)temp;

        }

        class complicated_water_bottle
        {
            int height;
            int weight;
            int capacity;

            //constructor that will assign default values to the three fields in the class
            public complicated_water_bottle()
            {
                height = 10;
                weight = 20;
                capacity = 1000;
            }

            //now this method will allow explicit conversions to int type
            public static implicit operator int(complicated_water_bottle bottle)
            {
                //when casting happens, the value that is set in the object's capacity will be given to the target int type
                return bottle.capacity;
            }

            //this method will allow implicit conversions to int type
            public static explicit operator double(complicated_water_bottle bottle)
            {
                //when casting happens, the value that is set in the object's capacity will be given to the target int type
                //unlike in implicity above, the casting must be explicit
                return bottle.capacity;
            }
        }
    }
}
